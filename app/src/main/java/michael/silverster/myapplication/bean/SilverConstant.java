package michael.silverster.myapplication.bean;

import java.util.ArrayList;
import java.util.List;

import michael.silverster.myapplication.R;

public class SilverConstant {

    public static List<SummonerDefaultData> getDefaulSummoners(){
        List<SummonerDefaultData> summonerDefaultData = new ArrayList<>();
        summonerDefaultData.add(new SummonerDefaultData("Erick Robert","v-IFe3h6dFSXNXOxXDw1KA8opLVSlkbi2Yd488B0iwia2v4", R.drawable.erick,"Jungle , Top"));
        summonerDefaultData.add(new SummonerDefaultData("Rojo Rajaobelina","yTFexjziP5IGXk4XDq_OyfG96LRKRqoZT6W8VPgKqMITg2Q", R.drawable.rojo,"ADC , Jungle"));
        summonerDefaultData.add(new SummonerDefaultData("Michael Rabenantoandro","y1SopUrRvMlLcqm0HtqvUkN1r_51UkpceKBle8u36ZSfxnU", R.drawable.michael,"Top ,Supp"));
        summonerDefaultData.add(new SummonerDefaultData("Cotte Marc Anthonio","pD0thPlcPrI162j15UxUFuWXr-Vb9E2mZxQcKosSQryvCfw", R.drawable.antho,"Mid , Top"));
        summonerDefaultData.add(new SummonerDefaultData("Tiavina Rajaobelina","MmFh6vxuRclsvOwkP4XOqV_7TcSaMhsGhHD_oYoO6ZcOCyg", R.drawable.titine,"Top, Mid"));
        summonerDefaultData.add(new SummonerDefaultData("Haga Rajaobelina","u9fRuH9sO--0VRybnJPwxrdSDLS91PnVsfNPsKysGDNiAxw", R.drawable.haga,"Top , ADC"));
        summonerDefaultData.add(new SummonerDefaultData("Rado Rajaobelina","PqOBMb5AuHLd15QVeaPhgg8_khgDaMmDnmxJwqCVuF0C3kU", R.drawable.rado,"Supp, Top"));
        summonerDefaultData.add(new SummonerDefaultData("Antso Ratsimivony","xLiPHwfg6IbaN-dTVQq5Xpu7OTnJ5KCaR-U1ZjSUNu_HN7M", R.drawable.aken_prime,"Jungle , Support"));





        return summonerDefaultData;
    }
}
