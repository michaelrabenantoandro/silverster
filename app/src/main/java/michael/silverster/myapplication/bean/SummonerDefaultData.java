package michael.silverster.myapplication.bean;

public class SummonerDefaultData {
    private String name;
    private String summonerId;
    private int playerPics;
    private String playerPost;


    public SummonerDefaultData() {
    }

    public SummonerDefaultData(String name, String summonerId, int playerPics, String playerPost) {
        this.name = name;
        this.summonerId = summonerId;
        this.playerPics = playerPics;
        this.playerPost = playerPost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummonerId() {
        return summonerId;
    }

    public void setSummonerId(String summonerId) {
        this.summonerId = summonerId;
    }

    public int getPlayerPics() {
        return playerPics;
    }

    public void setPlayerPics(int playerPics) {
        this.playerPics = playerPics;
    }

    public String getPlayerPost() {
        return playerPost;
    }

    public void setPlayerPost(String playerPost) {
        this.playerPost = playerPost;
    }
}
