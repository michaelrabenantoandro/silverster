package michael.silverster.myapplication.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class SummonerBean implements Parcelable {

    private String playerName;
    private String summonerName;
    private Integer summonerLevel;
    private Integer rank_id;
    private int playerPicture;
    private Integer soloQueuewinRate;
    private String mainPostes;
    private int mainPosteImage;
    private int medalPicture;
    private int medalFlexPicture;

    private String soloQueueRank;
    private Integer soloQueueWin;
    private Integer soloQueueLoose;
    private Integer soloQueueLeaguePoints;
    private Boolean SoloQueueIsHotStreak;

    private String flexQueueRank;
    private Integer flexQueueWin;
    private Integer flexQueueLoose;
    private Integer flexQueueLeaguePoints;
    private Boolean flexQueueIsHotStreak;


    private Integer normaQueueWin;
    private Integer normalQueueLoose;

    private Double soloQueueWinRate;
    private Double flexQueueWinRate;
    private Double normalQueueWinRate;

    private Boolean hasWonLastGame;


    public SummonerBean() {
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }

    public Integer getSummonerLevel() {
        return summonerLevel;
    }

    public void setSummonerLevel(Integer summonerLevel) {
        this.summonerLevel = summonerLevel;
    }

    public Integer getRank_id() {
        return rank_id;
    }

    public void setRank_id(Integer rank_id) {
        this.rank_id = rank_id;
    }

    public int getPlayerPicture() {
        return playerPicture;
    }

    public void setPlayerPicture(int playerPicture) {
        this.playerPicture = playerPicture;
    }

    public Integer getSoloQueuewinRate() {
        return soloQueuewinRate;
    }

    public void setSoloQueuewinRate(Integer soloQueuewinRate) {
        this.soloQueuewinRate = soloQueuewinRate;
    }

    public String getMainPostes() {
        return mainPostes;
    }

    public void setMainPostes(String mainPostes) {
        this.mainPostes = mainPostes;
    }

    public int getMainPosteImage() {
        return mainPosteImage;
    }

    public void setMainPosteImage(int mainPosteImage) {
        this.mainPosteImage = mainPosteImage;
    }

    public int getMedalPicture() {
        return medalPicture;
    }

    public void setMedalPicture(int medalPicture) {
        this.medalPicture = medalPicture;
    }

    public int getMedalFlexPicture() {
        return medalFlexPicture;
    }

    public void setMedalFlexPicture(int medalFlexPicture) {
        this.medalFlexPicture = medalFlexPicture;
    }

    public String getSoloQueueRank() {
        return soloQueueRank;
    }

    public void setSoloQueueRank(String soloQueueRank) {
        this.soloQueueRank = soloQueueRank;
    }

    public Integer getSoloQueueWin() {
        return soloQueueWin;
    }

    public void setSoloQueueWin(Integer soloQueueWin) {
        this.soloQueueWin = soloQueueWin;
    }

    public Integer getSoloQueueLoose() {
        return soloQueueLoose;
    }

    public void setSoloQueueLoose(Integer soloQueueLoose) {
        this.soloQueueLoose = soloQueueLoose;
    }

    public Integer getSoloQueueLeaguePoints() {
        return soloQueueLeaguePoints;
    }

    public void setSoloQueueLeaguePoints(Integer soloQueueLeaguePoints) {
        this.soloQueueLeaguePoints = soloQueueLeaguePoints;
    }

    public Boolean getSoloQueueIsHotStreak() {
        return SoloQueueIsHotStreak;
    }

    public void setSoloQueueIsHotStreak(Boolean soloQueueIsHotStreak) {
        SoloQueueIsHotStreak = soloQueueIsHotStreak;
    }

    public String getFlexQueueRank() {
        return flexQueueRank;
    }

    public void setFlexQueueRank(String flexQueueRank) {
        this.flexQueueRank = flexQueueRank;
    }

    public Integer getFlexQueueWin() {
        return flexQueueWin;
    }

    public void setFlexQueueWin(Integer flexQueueWin) {
        this.flexQueueWin = flexQueueWin;
    }

    public Integer getFlexQueueLoose() {
        return flexQueueLoose;
    }

    public void setFlexQueueLoose(Integer flexQueueLoose) {
        this.flexQueueLoose = flexQueueLoose;
    }

    public Integer getFlexQueueLeaguePoints() {
        return flexQueueLeaguePoints;
    }

    public void setFlexQueueLeaguePoints(Integer flexQueueLeaguePoints) {
        this.flexQueueLeaguePoints = flexQueueLeaguePoints;
    }

    public Boolean getFlexQueueIsHotStreak() {
        return flexQueueIsHotStreak;
    }

    public void setFlexQueueIsHotStreak(Boolean flexQueueIsHotStreak) {
        this.flexQueueIsHotStreak = flexQueueIsHotStreak;
    }

    public Integer getNormaQueueWin() {
        return normaQueueWin;
    }

    public void setNormaQueueWin(Integer normaQueueWin) {
        this.normaQueueWin = normaQueueWin;
    }

    public Integer getNormalQueueLoose() {
        return normalQueueLoose;
    }

    public void setNormalQueueLoose(Integer normalQueueLoose) {
        this.normalQueueLoose = normalQueueLoose;
    }

    public Double getSoloQueueWinRate() {
        return soloQueueWinRate;
    }

    public void setSoloQueueWinRate(Double soloQueueWinRate) {
        this.soloQueueWinRate = soloQueueWinRate;
    }

    public Double getFlexQueueWinRate() {
        return flexQueueWinRate;
    }

    public void setFlexQueueWinRate(Double flexQueueWinRate) {
        this.flexQueueWinRate = flexQueueWinRate;
    }

    public Double getNormalQueueWinRate() {
        return normalQueueWinRate;
    }

    public void setNormalQueueWinRate(Double normalQueueWinRate) {
        this.normalQueueWinRate = normalQueueWinRate;
    }

    public Boolean getHasWonLastGame() {
        return hasWonLastGame;
    }

    public void setHasWonLastGame(Boolean hasWonLastGame) {
        this.hasWonLastGame = hasWonLastGame;
    }

    public static Creator<SummonerBean> getCREATOR() {
        return CREATOR;
    }

    protected SummonerBean(Parcel in) {
        playerName = in.readString();
        summonerName = in.readString();
        if (in.readByte() == 0) {
            summonerLevel = null;
        } else {
            summonerLevel = in.readInt();
        }
        if (in.readByte() == 0) {
            rank_id = null;
        } else {
            rank_id = in.readInt();
        }
        playerPicture = in.readInt();
        if (in.readByte() == 0) {
            soloQueuewinRate = null;
        } else {
            soloQueuewinRate = in.readInt();
        }
        mainPostes = in.readString();
        mainPosteImage = in.readInt();
        medalPicture = in.readInt();
        medalFlexPicture = in.readInt();
        soloQueueRank = in.readString();
        if (in.readByte() == 0) {
            soloQueueWin = null;
        } else {
            soloQueueWin = in.readInt();
        }
        if (in.readByte() == 0) {
            soloQueueLoose = null;
        } else {
            soloQueueLoose = in.readInt();
        }
        if (in.readByte() == 0) {
            soloQueueLeaguePoints = null;
        } else {
            soloQueueLeaguePoints = in.readInt();
        }
        byte tmpSoloQueueIsHotStreak = in.readByte();
        SoloQueueIsHotStreak = tmpSoloQueueIsHotStreak == 0 ? null : tmpSoloQueueIsHotStreak == 1;
        flexQueueRank = in.readString();
        if (in.readByte() == 0) {
            flexQueueWin = null;
        } else {
            flexQueueWin = in.readInt();
        }
        if (in.readByte() == 0) {
            flexQueueLoose = null;
        } else {
            flexQueueLoose = in.readInt();
        }
        if (in.readByte() == 0) {
            flexQueueLeaguePoints = null;
        } else {
            flexQueueLeaguePoints = in.readInt();
        }
        byte tmpFlexQueueIsHotStreak = in.readByte();
        flexQueueIsHotStreak = tmpFlexQueueIsHotStreak == 0 ? null : tmpFlexQueueIsHotStreak == 1;
        if (in.readByte() == 0) {
            normaQueueWin = null;
        } else {
            normaQueueWin = in.readInt();
        }
        if (in.readByte() == 0) {
            normalQueueLoose = null;
        } else {
            normalQueueLoose = in.readInt();
        }
        if (in.readByte() == 0) {
            soloQueueWinRate = null;
        } else {
            soloQueueWinRate = in.readDouble();
        }
        if (in.readByte() == 0) {
            flexQueueWinRate = null;
        } else {
            flexQueueWinRate = in.readDouble();
        }
        if (in.readByte() == 0) {
            normalQueueWinRate = null;
        } else {
            normalQueueWinRate = in.readDouble();
        }
        byte tmpHasWonLastGame = in.readByte();
        hasWonLastGame = tmpHasWonLastGame == 0 ? null : tmpHasWonLastGame == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(playerName);
        dest.writeString(summonerName);
        if (summonerLevel == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(summonerLevel);
        }
        if (rank_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(rank_id);
        }
        dest.writeInt(playerPicture);
        if (soloQueuewinRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(soloQueuewinRate);
        }
        dest.writeString(mainPostes);
        dest.writeInt(mainPosteImage);
        dest.writeInt(medalPicture);
        dest.writeInt(medalFlexPicture);
        dest.writeString(soloQueueRank);
        if (soloQueueWin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(soloQueueWin);
        }
        if (soloQueueLoose == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(soloQueueLoose);
        }
        if (soloQueueLeaguePoints == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(soloQueueLeaguePoints);
        }
        dest.writeByte((byte) (SoloQueueIsHotStreak == null ? 0 : SoloQueueIsHotStreak ? 1 : 2));
        dest.writeString(flexQueueRank);
        if (flexQueueWin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(flexQueueWin);
        }
        if (flexQueueLoose == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(flexQueueLoose);
        }
        if (flexQueueLeaguePoints == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(flexQueueLeaguePoints);
        }
        dest.writeByte((byte) (flexQueueIsHotStreak == null ? 0 : flexQueueIsHotStreak ? 1 : 2));
        if (normaQueueWin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(normaQueueWin);
        }
        if (normalQueueLoose == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(normalQueueLoose);
        }
        if (soloQueueWinRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(soloQueueWinRate);
        }
        if (flexQueueWinRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(flexQueueWinRate);
        }
        if (normalQueueWinRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(normalQueueWinRate);
        }
        dest.writeByte((byte) (hasWonLastGame == null ? 0 : hasWonLastGame ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SummonerBean> CREATOR = new Creator<SummonerBean>() {
        @Override
        public SummonerBean createFromParcel(Parcel in) {
            return new SummonerBean(in);
        }

        @Override
        public SummonerBean[] newArray(int size) {
            return new SummonerBean[size];
        }
    };
}
