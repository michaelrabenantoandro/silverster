package michael.silverster.myapplication.bean;

import michael.silverster.myapplication.R;

public enum Medals {

    IRON(R.drawable.caca,"IRON"),
    NULL(R.drawable.caca,"null"),
    BRONZE(R.drawable.bronze,"BRONZE"),
    SILVER(R.drawable.silver,"SILVER"),
    GOLD(R.drawable.gold,"GOLD"),
    DIAMANT(R.drawable.diamant,"DIAMANT"),;

    private int image;
    private String tiers;


    Medals(int image, String tiers) {
        this.image = image;
        this.tiers = tiers;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTiers() {
        return tiers;
    }

    public void setTiers(String tiers) {
        this.tiers = tiers;
    }
}
