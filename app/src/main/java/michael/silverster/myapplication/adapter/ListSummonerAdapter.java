package michael.silverster.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import michael.silverster.myapplication.R;
import michael.silverster.myapplication.bean.SummonerBean;
import michael.silverster.myapplication.holder.SummonerListHolder;

public class ListSummonerAdapter extends RecyclerView.Adapter<SummonerListHolder> {

    private Context context;
    private List<SummonerBean> summonerBeans;

    public ListSummonerAdapter(Context context, List<SummonerBean> summonerBeans) {
        this.context = context;
        this.summonerBeans = summonerBeans;
    }

    @NonNull
    @Override
    public SummonerListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_summoner, parent, false);
        return new SummonerListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SummonerListHolder holder, int position) {
        SummonerBean summonerBean = summonerBeans.get(position);
        try {
            holder.setComponents(summonerBean, context);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return summonerBeans.size();
    }
}
