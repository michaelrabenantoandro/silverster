package michael.silverster.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import michael.silverster.myapplication.AppBaseActivity;
import michael.silverster.myapplication.R;
import michael.silverster.myapplication.adapter.ListSummonerAdapter;
import michael.silverster.myapplication.bean.SummonerBean;

public class RankingActivity extends AppBaseActivity {

    @BindView(R.id.recycler_View_Summoner_list)
    RecyclerView recyclerViewSummonerList;
    private ListSummonerAdapter listSummonerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        ButterKnife.bind(this);
        Intent i = getIntent();
        List<SummonerBean> summonerBeanArrayList = getIntent().getParcelableArrayListExtra("summoner");
        listSummonerAdapter = new ListSummonerAdapter(this, summonerBeanArrayList);
        recyclerViewSummonerList.setHasFixedSize(true);
        recyclerViewSummonerList.setItemViewCacheSize(20);
        recyclerViewSummonerList.setDrawingCacheEnabled(true);
        recyclerViewSummonerList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerViewSummonerList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSummonerList.setAdapter(listSummonerAdapter);
    }
}
