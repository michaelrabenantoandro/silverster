package michael.silverster.myapplication.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import michael.silverster.myapplication.AppBaseActivity;
import michael.silverster.myapplication.Async.CallRiotApiAsyncTask;
import michael.silverster.myapplication.R;

public class Splashscreen extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //Remove title ba

        setContentView(R.layout.activity_splashscreen);
        new CallRiotApiAsyncTask(this).execute();

    }
}
