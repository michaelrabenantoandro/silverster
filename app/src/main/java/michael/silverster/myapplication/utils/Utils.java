package michael.silverster.myapplication.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import michael.silverster.myapplication.AppBaseActivity;

public class Utils {
    /**
     * Start activity with bundle parameters
     *
     * @param activity
     * @param context
     * @param datas
     */
    public static void startThisActivity(Class<? extends AppBaseActivity> activity, Context context, Bundle datas) {
        Intent toStart = new Intent(context, activity);
        if (datas != null) {
            toStart.putExtras(datas);
        }
        context.startActivity(toStart);
    }






}
