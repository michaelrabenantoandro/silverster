package michael.silverster.myapplication.holder;

import android.content.Context;
import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import michael.silverster.myapplication.R;
import michael.silverster.myapplication.bean.SummonerBean;

public class SummonerListHolder extends RecyclerView.ViewHolder  {

    private TextView playerName;
    private TextView summonerName;
    private TextView summonerLvl;
    private TextView mainPost;
    private TextView winrate;
    private TextView soloQueue;
    private TextView flexQueue;
    private ImageView imageViewSoloMedal;
    private ImageView imageViewFlexMedal;
    private CircleImageView circleImageView;


    public SummonerListHolder(@NonNull View itemView) {

        super(itemView);
        playerName = itemView.findViewById(R.id.textview_player_name);
        summonerName = itemView.findViewById(R.id.textview_summoner_name);
        summonerLvl = itemView.findViewById(R.id.textview_summoner_level);
        mainPost = itemView.findViewById(R.id.textview_main_post);
        winrate = itemView.findViewById(R.id.textview_winrate);
        soloQueue = itemView.findViewById(R.id.textview_solo_queue);
        flexQueue = itemView.findViewById(R.id.textview_flex_queue);
        circleImageView = itemView.findViewById(R.id.summoner_picture);
        imageViewSoloMedal = itemView.findViewById(R.id.image_solo_medal);
        imageViewFlexMedal = itemView.findViewById(R.id.image_flex_medal);

    }

    public void setComponents(SummonerBean summonerBean, Context context){
        playerName.setText(summonerBean.getPlayerName());
        summonerName.setText(summonerBean.getSummonerName());
        mainPost.setText(summonerBean.getMainPostes());
        summonerLvl.setText("Level: " + String.valueOf(summonerBean.getSummonerLevel()) + " - " + summonerBean.getSoloQueueLeaguePoints() + " lp");
        int numberGames = summonerBean.getSoloQueueWin() + summonerBean.getSoloQueueLoose();
        winrate.setText("Winrate Solo: " + summonerBean.getSoloQueuewinRate() +"% | " + String.valueOf(numberGames) + "games");
        soloQueue.setText(" Solo: " + summonerBean.getSoloQueueRank());
        flexQueue.setText(" Flex: " + summonerBean.getFlexQueueRank());
        Picasso.get().load(summonerBean.getPlayerPicture())
                .resize(150,150)
                .onlyScaleDown()
                .into(circleImageView);

        Picasso.get().load(summonerBean.getMedalPicture())
                .resize(150,150)
                .onlyScaleDown()
                .into(imageViewSoloMedal);
        Picasso.get().load(summonerBean.getMedalFlexPicture())
                .resize(150,150)
                .onlyScaleDown()
                .into(imageViewFlexMedal);
    }

    public TextView getPlayerName() {
        return playerName;
    }

    public void setPlayerName(TextView playerName) {
        this.playerName = playerName;
    }

    public TextView getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(TextView summonerName) {
        this.summonerName = summonerName;
    }

    public TextView getSummonerLvl() {
        return summonerLvl;
    }

    public void setSummonerLvl(TextView summonerLvl) {
        this.summonerLvl = summonerLvl;
    }

    public TextView getMainPost() {
        return mainPost;
    }

    public void setMainPost(TextView mainPost) {
        this.mainPost = mainPost;
    }

    public TextView getWinrate() {
        return winrate;
    }

    public void setWinrate(TextView winrate) {
        this.winrate = winrate;
    }

    public TextView getSoloQueue() {
        return soloQueue;
    }

    public void setSoloQueue(TextView soloQueue) {
        this.soloQueue = soloQueue;
    }

    public TextView getFlexQueue() {
        return flexQueue;
    }

    public void setFlexQueue(TextView flexQueue) {
        this.flexQueue = flexQueue;
    }
}
