package michael.silverster.myapplication.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.league.constant.LeagueQueue;
import net.rithms.riot.api.endpoints.league.dto.LeagueEntry;
import net.rithms.riot.api.endpoints.summoner.dto.Summoner;
import net.rithms.riot.constant.Platform;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import michael.silverster.myapplication.activity.RankingActivity;
import michael.silverster.myapplication.bean.Medals;
import michael.silverster.myapplication.bean.SilverConstant;
import michael.silverster.myapplication.bean.SummonerBean;
import michael.silverster.myapplication.bean.SummonerDefaultData;
import michael.silverster.myapplication.utils.Utils;

public class CallRiotApiAsyncTask extends AsyncTask<Void, Void, List<SummonerBean>> {

    private Context context;

    public CallRiotApiAsyncTask(Context context) {
        this.context = context;
    }

    public CallRiotApiAsyncTask() {
    }

    @Override
    protected List<SummonerBean> doInBackground(Void... voids) {
        ApiConfig config = new ApiConfig().setKey("RGAPI-eb53c02f-44ef-4f5a-ba75-75ff18d91a2b");
        RiotApi api = new RiotApi(config);
        List<SummonerBean> summonerBeanList = new ArrayList<>();
        List<SummonerDefaultData> summonerDefaultData = SilverConstant.getDefaulSummoners();
        try {
            for (int i = 0; i < summonerDefaultData.size(); i++) {
                SummonerBean summonerBean = new SummonerBean();
                Summoner summoner = api.getSummoner(Platform.EUW, summonerDefaultData.get(i).getSummonerId());
                Set<LeagueEntry> leagues = api.getLeagueEntriesBySummonerId(Platform.EUW, summoner.getId());
                for (LeagueEntry league : leagues) {
                    if (league.getQueueType().equals(LeagueQueue.RANKED_SOLO_5x5.name())) {
                        summonerBean.setSoloQueueRank(league.getTier() + " " + league.getRank());
                        summonerBean.setSoloQueueWin(league.getWins());
                        summonerBean.setSoloQueueLoose(league.getLosses());
                        summonerBean.setSoloQueueLeaguePoints(league.getLeaguePoints());
                        summonerBean.setSoloQueuewinRate((100 * league.getWins()) / (league.getWins() + league.getLosses()));
                        for (Medals medaille : Medals.values()) {
                            if (medaille.getTiers().equalsIgnoreCase(league.getTier())) {
                                summonerBean.setMedalPicture(medaille.getImage());
                            }
                        }
                    }
                    if (league.getQueueType().equals(LeagueQueue.RANKED_FLEX_SR.name())) {
                        summonerBean.setFlexQueueRank(league.getTier() + " " + league.getRank());
                        summonerBean.setFlexQueueWin(league.getWins());
                        summonerBean.setFlexQueueLoose(league.getLosses());
                        summonerBean.setFlexQueueLeaguePoints(league.getLeaguePoints());
                        for (Medals medaille : Medals.values()) {
                            if (medaille.getTiers().equalsIgnoreCase(league.getTier())) {
                                summonerBean.setMedalFlexPicture(medaille.getImage());
                            }
                        }
                    }
                }

                summonerBean.setPlayerName(summonerDefaultData.get(i).getName());
                summonerBean.setSummonerName(summoner.getName());
                summonerBean.setMainPostes(summonerDefaultData.get(i).getPlayerPost());
                summonerBean.setSummonerLevel(summoner.getSummonerLevel());
                summonerBean.setPlayerPicture(summonerDefaultData.get(i).getPlayerPics());
                summonerBeanList.add(summonerBean);
            }
        } catch (RiotApiException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return summonerBeanList;
    }

    @Override
    protected void onPostExecute(List<SummonerBean> summonerBeans) {
        super.onPostExecute(summonerBeans);
        Bundle data = new Bundle();
        ArrayList<SummonerBean> summonerBeanList = (ArrayList<SummonerBean>) summonerBeans;
        data.putParcelableArrayList("summoner", summonerBeanList);
        Utils.startThisActivity(RankingActivity.class, context, data);
    }
}

